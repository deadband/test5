﻿using System;

namespace KatalogKomiksow
{
    internal class Program
    {
        //1. Adapter
        //2. Strategia

        private static void Main(string[] args)
        {
            var loginPage = new LoginPage(Console.WindowWidth / 2 - 30, Console.WindowHeight / 2 - 5, 60, 10);

            var currentWindow = loginPage;


            while (true)
            {
                if (Console.KeyAvailable) currentWindow.HandleFocus(Console.ReadKey(true));
                currentWindow.Render();
            }
        }
    }
}