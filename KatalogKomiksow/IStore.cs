﻿namespace KatalogKomiksow
{
    public interface IStore
    {
        void Sell(ComicBook comicBook);
        void AddToCatalog(ComicBook comicBook);
    }
}