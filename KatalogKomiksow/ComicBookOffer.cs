﻿namespace KatalogKomiksow
{
    public class ComicBookOffer
    {
        public ComicBook ComicBook { get; set; }
        public decimal Price { get; set; }
    }
}