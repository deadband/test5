﻿namespace KatalogKomiksow
{
    internal interface IRestriction
    {
        bool IsLimitedForPerson(Person person);
    }
}