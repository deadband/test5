﻿namespace KatalogKomiksow
{
    public class AdultsWestern : ComicBook
    {
        public override bool IsLimitedForPerson(Person person)
        {
            return person.Age < 18;
        }
    }
}