﻿using System.Text.RegularExpressions;

namespace KatalogKomiksow
{
    public class PhoneValidator
    {
        public static bool IsCorrectNumber(string phoneNumber)
        {
            var matchPattern = @"\+?\d{0,3}[\s-]?\d{3}[\s-]??\d{3}[\s-]?\d{3}\b";
            return Regex.Match(phoneNumber, matchPattern).Success;
        }
    }
}