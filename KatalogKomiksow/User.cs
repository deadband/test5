﻿using System.Collections.Generic;

namespace KatalogKomiksow
{
    public class User
    {
        public AccountProvider AccountProvider { get; set; }
        public List<ComicBook> ComicBooks { get; set; }
    }
}