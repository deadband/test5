﻿using Rox.Console;

namespace KatalogKomiksow
{
    public class LoginPage : ConsoleWindow
    {
        public LoginPage(int left, int top, int width, int height) : base(left, top, width, height)
        {
            Title = "Testy są WAŻNE";
            var line1TextBox = new TextBox(1, 2, width - 2, 1){Label = "Powodzenia w rozwiązaniu testów :)  ", FocusIndex = 1};
            var line3TextBox = new TextBox(1, 3, width - 2, 1) { Label = "Treść zadania jest w Tests.ComicBookTests", FocusIndex = 1 };
            var line2TextBox = new TextBox(1, 4, width - 2, 5) { Label = "Ps Używam tu Coposition Pattern.", FocusIndex = 1 };

            AddControl(line1TextBox);
            AddControl(line2TextBox);
            AddControl(line3TextBox);


        }
    }
}