﻿using System;

namespace KatalogKomiksow
{
    public class ComicBookFactory
    {
        public static ComicBook GetAdultWestern()
        {
            return new AdultsWestern();
        }

        public static ComicBook GetKidsWestern()
        {
            return new KidsWestern();
        }
    }
}