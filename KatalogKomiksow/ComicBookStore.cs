﻿using System;
using System.Collections.Generic;
using KatalogKomiksow.Providers;

namespace KatalogKomiksow
{
    public class ComicBookStore : IStore
    {
        public List<ComicBookOffer> Catalog { get; set; }

        public void AddToCatalog(ComicBook comicBook)
        {
            throw new NotImplementedException();
        }

        public void Sell(ComicBook comicBook)
        {
            throw new NotImplementedException();
        }

        public string Buy(ComicBook comicBook, PaymentMethod paymentMethod)
        {
            var paymentProvider = PaymentProviderFactory.GetPaymentProvider(paymentMethod);
            return paymentProvider.Pay(comicBook.Price);
        }
    }

    public static class PaymentProviderFactory
    {
        public static PaymentProvider GetPaymentProvider(PaymentMethod paymentMethod)
        {
            switch (paymentMethod)
            {
                case PaymentMethod.PayPal:
                    return new PayPallAdapter();
                case PaymentMethod.PayU:
                    return new PayUAdapter();
            }
            return  new PaymentProvider();
        }
    }

    public class PayPallAdapter : PaymentProvider
    {
        private readonly PayPallProvider _payPallProvider;
        public PayPallAdapter()
        {
            _payPallProvider = new PayPallProvider();
        }

        public override string Pay(decimal amount)
        {
            return _payPallProvider.PayByPayPallRequest(amount);
        }
    }

    public class PayUAdapter : PaymentProvider
    {
        private readonly PayUProvider _payUProvider;
        public PayUAdapter()
        {
            _payUProvider = new PayUProvider(); 
        }

        public override string Pay(decimal amount)
        {
            return _payUProvider.PayByPayURequest(amount, 0);
        }
    }

    public class PaymentProvider
    {
        public virtual string Pay(decimal amount)
        {
            return $"Default Payment Provider: Success{amount} ";
        }
    }
}