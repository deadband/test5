﻿namespace KatalogKomiksow
{
    public class KidsWestern : ComicBook
    {
        public override bool IsLimitedForPerson(Person person)
        {
            return person.Age < 2;
        }
    }
}