﻿using System;

namespace KatalogKomiksow
{
    public class ComicBook : ISellable, ILendable, IRestriction
    {
        public Person Author { get; set; }
        public User Owner { get; set; }
        public decimal Price { get; set; }

        public virtual bool IsLimitedForPerson(Person person)
        {
            return false;
        }
    }
}