﻿using KatalogKomiksow;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace Tests
{
    /// <summary>
    /// Treść zadania:
    /// 1. Z pomocą strategii naprawić testy dotyczące limitowania dostępu do komiksów ze względu na wiek osoby.
    ///     a) LimitationTestLimitedOnlyForAdult()
    ///     b) LimitationTestAvailableForYoung()
    /// 2. Z pomocą adaptera zaadaptować PayPallProvider oraz PayUProvider tak by dało się z nich skorzystać jak z domyślnego PaymentProvidera
    ///     a) ComicBookPayByPayPal()
    ///     b) ComicBookPayByPayU()
    /// 3. Korzystając z Regexa zwalidować numery telefonów.
    ///     a) PhoneVerify()
    /// W realizacji zadania nie należy zmieniać nazw metod w Providerach. Czas realizacji zadania 60 minut.
    ///  

    /// </summary>
    [TestClass]
    public class ComicBookTests
    {
        [TestMethod]
        public void LimitationTestLimitedOnlyForAdult()
        {
            var adultComicBook = ComicBookFactory.GetAdultWestern();
            Assert.IsTrue(adultComicBook.IsLimitedForPerson(new Person {Age = 16}));
        }

        [TestMethod]
        public void LimitationTestAvailableForYoung()
        {
            var kidsComicBook = ComicBookFactory.GetKidsWestern();
            Assert.IsFalse(kidsComicBook.IsLimitedForPerson(new Person {Age = 16}));
        }

        [TestMethod]
        public void ComicBookPayByPayPal()
        {
            var store = new ComicBookStore();
            var response = store.Buy(new ComicBook {Price = 12}, PaymentMethod.PayPal);
            Assert.AreEqual("PayPal:12", response);
        }

        [TestMethod]
        public void ComicBookPayByPayU()
        {
            var store = new ComicBookStore();
            var response = store.Buy(new ComicBook {Price = 13}, PaymentMethod.PayU);
            Assert.AreEqual("PayU:13|Discount:0", response);
        }

        [TestMethod]
        public void PhoneVerify()
        {
            Assert.IsTrue(PhoneValidator.IsCorrectNumber("+48 111 111 111"));
            Assert.IsTrue(PhoneValidator.IsCorrectNumber("111111111"));
            Assert.IsTrue(PhoneValidator.IsCorrectNumber("111 111 111"));

            Assert.IsFalse(PhoneValidator.IsCorrectNumber("+48 111 111 BA1"));
            Assert.IsFalse(PhoneValidator.IsCorrectNumber("+48 111 111 11111111"));
        }
    }
}