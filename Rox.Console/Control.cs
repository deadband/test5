﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Rox.Console
{
    public abstract class Control
    {
        protected Control(int left, int top, int width, int height)
        {
            Top = top;
            Left = left;
            Width = width;
            Height = height;
        }

        public bool Focused { internal get; set; }
        public Control Parent { private get; set; }
        public bool NeedToRender { private get; set; } = true;
        public virtual bool SkipKey { get; set; } = true;
        public int FocusIndex { get; set; } = 0;

        protected int Height { get; }

        protected int Left { get; }

        protected internal int Top { protected get; set; }

        protected int Width { get; }

        public virtual void Render(int left, int top)
        {
            if (NeedToRender)
            {
                RenderControlContent(left + Left, top + Top);
                NeedToRender = false;
            }
        }

        protected abstract void RenderControlContent(int left, int top);

        public virtual void HandleFocus(ConsoleKeyInfo keyInfo)
        {
            if (keyInfo.Key == ConsoleKey.Tab) ChangeFocus();
        }

        protected virtual void ChangeFocus()
        {
            Parent?.ChangeFocus();
        }
    }

    public abstract class Form : CompositeControl
    {
        protected Form(int left, int top, int width, int height) : base(left, top, width, height)
        {
        }

        protected Control FocusedControl { private get; set; }

        public override bool SkipKey
        {
            get => FocusedControl?.SkipKey ?? true;
            set => base.SkipKey = value;
        }

        public override void HandleFocus(ConsoleKeyInfo keyInfo)
        {
            FocusedControl?.HandleFocus(keyInfo);
        }

        protected override void ChangeFocus()
        {
            FocusedControl.Focused = false;
            FocusedControl.NeedToRender = true;
            FocusedControl = Controls.FirstOrDefault(p => p.FocusIndex > ((int) FocusedControl?.FocusIndex));
            FocusedControl = FocusedControl ?? Controls.FirstOrDefault();
            if (FocusedControl != null)
            {
                FocusedControl.Focused = true;
                FocusedControl.NeedToRender = true;
            }
        }
    }

    public abstract class CompositeControl : Control
    {
        protected CompositeControl(int left, int top, int width, int height) : base(left, top, width, height)
        {
        }

        protected List<Control> Controls { get; } = new List<Control>();

        public override void Render(int left, int top)
        {
            base.Render(left, top);
            foreach (var item in Controls) item.Render(left + Left, top + Top);
        }
    }
}