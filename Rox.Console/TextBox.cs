﻿using System;

namespace Rox.Console
{
    public sealed class TextBox : Control
    {
        public TextBox(int left, int top, int width, int height) : base(left, top, width, height)
        {
            SkipKey = false;
        }

        public string Label { private get; set; }
        private string Text { get; set; }

        protected override void RenderControlContent(int left, int top)
        {
            System.Console.BackgroundColor = ConsoleColor.Green;
            for (var i = 0; i < Height; i++)
            {
                System.Console.SetCursorPosition(left, top + i);
                System.Console.Write("".PadRight(Width));

            }
            System.Console.SetCursorPosition(left, top);

            System.Console.Write((Label
                                  + (Focused ? ">" : "") +
                                  Text).PadRight(Width).Substring(0, Width));

        }

        public override void HandleFocus(ConsoleKeyInfo keyInfo)
        {
            base.HandleFocus(keyInfo);
            if (keyInfo.Key == ConsoleKey.Tab) return;
            if (keyInfo.Key == ConsoleKey.Backspace)
            {
                if (Text.Length > 0) Text = Text.Remove(Text.Length - 1);
            }
            else
            {
                Text += keyInfo.KeyChar;
            }

            NeedToRender = true;
        }
    }
}