﻿using System;

namespace Rox.Console
{
    public class Panel : CompositeControl
    {
        public Panel(int left, int top, int width, int height) : base(left, top, width, height)
        {
        }

        protected override void RenderControlContent(int left, int top)
        {
            System.Console.BackgroundColor = ConsoleColor.Green;
            for (var i = 0; i < Height; i++)
            {
                System.Console.SetCursorPosition(left, top + i);
                System.Console.Write("".PadRight(Width));
            }
        }
    }
}