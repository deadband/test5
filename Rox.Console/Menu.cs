﻿using System;

namespace Rox.Console
{
    public class Menu : CompositeControl
    {
        private readonly int _height;
        private readonly int _width;
        private int _selectedIndex;

        public Menu(int top, int left, int width, int height) : base(top, left, width, height)
        {
            _width = width;
            _height = height;
        }

        private MenuItem SelectedItem { get; set; }

        protected override void RenderControlContent(int left, int top)
        {
            System.Console.BackgroundColor = ConsoleColor.DarkRed;
            for (var i = 0; i < _height; i++)
            {
                System.Console.SetCursorPosition(left, top + i);
                System.Console.Write("".PadRight(_width));
            }
        }

        public void AddItem(MenuItem menuItem)
        {
            menuItem.Parent = this;

            menuItem.Top = Controls.Count;
            Controls.Add(menuItem);
        }

        public override void HandleFocus(ConsoleKeyInfo keyInfo)
        {
            if (keyInfo.Key == ConsoleKey.UpArrow)
            {
                SelectedItem?.Unselect();
                _selectedIndex = _selectedIndex > 0 ? _selectedIndex - 1 : _selectedIndex;
                SelectedItem = (MenuItem) (_selectedIndex < Controls.Count ? Controls[_selectedIndex] : null as MenuItem);
                SelectedItem?.Select();
            }

            if (keyInfo.Key == ConsoleKey.DownArrow)
            {
                SelectedItem?.Unselect();
                _selectedIndex = _selectedIndex < Controls.Count - 1 ? _selectedIndex + 1 : _selectedIndex;
                SelectedItem = (MenuItem) (_selectedIndex < Controls.Count ? Controls[_selectedIndex] : null as MenuItem);
                SelectedItem?.Select();
            }
        }
    }

    public class MenuItem : Control
    {
        private readonly int _width;
        private bool _selected;

        public MenuItem(int left, int top, int width, int height, string text) : base(left, top, width, height)
        {
            _width = width;
            Text = text;
        }

        private string Text { get; }

        protected override void RenderControlContent(int left, int top)
        {
            System.Console.SetCursorPosition(left, top + Top);
            System.Console.BackgroundColor = _selected ? ConsoleColor.DarkMagenta : ConsoleColor.Magenta;
            System.Console.Write(Text.PadRight(_width));
        }

        internal void Select()
        {
            NeedToRender = true;
            _selected = true;
        }

        internal void Unselect()
        {
            NeedToRender = true;
            _selected = false;
        }
    }
}